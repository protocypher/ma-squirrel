from setuptools import setup


setup(
      name="Squirrel",
      version="0.1.0",
      packages=["squirrel"],
      url="https://bitbucket.org/protocypher/ma-squirrel",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A production inventory management application."
)

