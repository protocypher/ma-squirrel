# Squirrel

**Product Inventory**

Squirrel is a basic, text based, inventory management application that uses the
`cmd.Cmd` framework. It manages an inventory of products that have attributes
like price, quantity, time in storage, ... It can generate reports which can
project when products may go out of stock based on their depletion trend.
It can also produce other reports like the sum of value of items on hand.

